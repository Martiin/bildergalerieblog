<!DOCTYPE html>
<html>

<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <link href="../view/css/main.css" rel="stylesheet">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <title>Home</title>


</head>



<body>

    <header class=" navbar-inverse">
        <div class="container">
            <nav role="navigation">

                <!--Better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                    <a class="navbar-brand" href="">Galerie</a>
                </div>


                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                            <?php //Sichtbar wenn man angemeldet ist
            if(isset ($_SESSION["user_id"])){
                $description = "logout";              
                $path = "/user/doLogout";
            }else
            {   //sichtbar wenn man nicht angemeldet ist
                $description = "login";
                $path = "/user/login";
                ?>



                            <form class="navbar-form navbar-left" name="inputs" method="post" action="/User/doLogin">

                                <input class="form-control" type="text" name="username" class="form" placeholder="Benutzername" required>

                                <input class="form-control" type="password" name="password" class="tab" placeholder="Passwort" required>

                                <input type="submit" name="submit" value="Login" class="btn btn-default">
                                <input type="reset" name="reset" value="Reset" class="btn btn-default">


                            </form>

                            <script>
                                function check() {
                                    var p1 = document.getElementById("password").value;
                                    var p2 = document.getElementById("password2").value;

                                    if (p1 != p2) {
                                        alert('Passwörter stimmen nicht überein');
                                        return false;
                                    }
                                }

                            </script>









                            <?php  
            }
            
            
            //Wenn man nicht eingeloggt ist zeigt es auch Registrieren n
            if($description == "login"){
                ?><div class="navbar-form navbar-right dropdown">
                
                <button class="btn btn-danger dropdown-toggle" type="button" onclick="$('#posten')" title="klicken" data-toggle="dropdown">Registrieren <span class="caret"></span></button>

                                <form class="dropdown-menu " aria-labelledby="dropdownMenuLink" method="post" action="/user/doCreate" onsubmit="return check();">
                                
                                    
                                        <input id="us" class="form-control" type="text" name="username" placeholder="Benutzername" required>
                   
                                        
                                       
                                        <!--<input id="age" class="form-control" type="text" name="alter"  placeholder="Alter" required>-->
                      
                                        <input  class="form-control"type="password" name="password" id="password" placeholder="Passwort" required>
                                  
                                   
                                        <input class="form-control" type="password" name="password2" id="password2" placeholder="Passwort wiederholen" required>
                               

                                    <input type="submit" name="submit" value="Registrieren" class="btn btn-default">
                                    <input type="reset" name="reset" value="Reset" class="btn btn-default">
                                           

                                

                                </form>
     </div>
                     
                <?php
                
            }
         else{
            ?>

                            <div class="navbar-form navbar-left dropdown">
                                <button class="btn btn-danger dropdown-toggle" type="button" onclick="$('#posten')" title="klicken" data-toggle="dropdown">Neuer Post <span class="caret"></span></button>

                                <ul class="dropdown-menu">
                                    <li>
                                        <div id="posten" class="container">
                                            <form action="/Galerie/doCreate" method="POST" enctype="multipart/form-data">
                                                <p>
                                                    <input type="text" name="titel" class="tab" placeholder="Titel">
                                                </p>

                                                <p>
                                                    <textarea rows="4" cols="55" name="inhalt" class="textarea" placeholder="Inhalt"></textarea>
                                                </p>
                                                <p>
                                                    <input type="file" title="png, jpg, jpeg, gif" name="datei" size="60" maxlength="255">

                                                </p>

                                                <select name="kategorie" class="tab">
                <option value="%">Kategorie auswählen</option>
                <option value="Sport">Sport</option>
                <option value="Essen">Essen</option>
                <option value="Menschen">Menschen</option>
                <option value="Adventure">Adventure</option>
                <option value="Natur">Natur</option>
            </select>
                                                <p>
                                                    <input type="submit" name="submit" value="Posten" class="btn btn-default">
                                                </p>

                                            </form>
                                            <!-- Liest den geposteten Blog aus-->
                                        </div>
                                    </li>
                                </ul>


                            </div>
                        </li>

                        <form class="navbar-form navbar-left" action="/Galerie/doSortieren" <?php echo $_SERVER[ 'PHP_SELF']; ?> method="post">
                            <div class="form-group">
                                <?php $kategoriegesetzt = (isset($_POST['kategorie'])?$_POST['kategorie']:"");?>
                                <select name="kategorie" class="form-control">
                <option value="%"<?php if($kategoriegesetzt == "Alle") echo "selected=\"selected\""; ?> />Alle
                <option value="Sport"<?php if($kategoriegesetzt == "Sport") echo "selected=\"selected\""; ?> />Sport
                <option value="Essen"<?php if($kategoriegesetzt == "Essen") echo "selected=\"selected\""; ?> />Essen
                <option value="Menschen"<?php if($kategoriegesetzt == "Menschen") echo "selected=\"selected\""; ?> />Menschen
                <option value="Adventure"<?php if($kategoriegesetzt == "Adventure") echo "selected=\"selected\""; ?> />Adventure
                <option value="Natur"<?php if($kategoriegesetzt == "Natur") echo "selected=\"selected\""; ?> />Natur
            </select>
                            </div>
                            <input type="submit" title="klicken" name="sort" class="btn btn-default" value="Sortieren"><br/>
                        </form>



                    </ul>
                    <!--<form class="navbar-form navbar-left">
                            <div class="form-group">

                                <input type="text" class="form-control" placeholder="Search">
                            </div>
                            <button type="submit" class="btn btn-default">Submit</button>
                        </form>-->
                    <ul class="nav navbar-nav navbar-right">

                        <li>



                            <?php echo '<li class="active"><a>'.'Hallo '.$_SESSION['username'].'</a></li>';
                        //Bezieht sich auf SESSION während man angemeldet ist
                        echo '<li><a href="'.$path.'">'.$description.'</a></li>';
             
            ?>
                        </li>

                    </ul>
                </div>
                <!-- /.navbar-collapse -->

            </nav>



            <?php
         }
            
            
            ?>
        </div>
    </header>
