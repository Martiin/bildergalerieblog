<?php
if(isset ($_GET["user_id"])&&isset ($_GET["titel"])&&isset ($_GET["inhalt"])){
    
    $userid = $_GET["user_id"];
    $titel = $_GET["titel"];
    $inhalt = $_GET["inhalt"];

        
}

?>
<header class=" navbar-inverse">
    <div class="container">
        <nav role="navigation">

            <!--Better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/Galerie/index">Galerie</a>
            </div>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <?php //Sichtbar wenn man angemeldet ist
            if(isset ($_SESSION["user_id"])){
                $description = "logout";              
                $path = "/user/doLogout";
            }else
            {   //sichtbar wenn man nicht angemeldet ist
                $description = "login";
                $path = "/user/login";
                
            }
            
            
            //Wenn man nicht eingeloggt ist zeigt es auch Registrieren n
            if($description == "login"){
                echo '<li><a href="/user/create">Registrieren</a></li>';
                echo '<li><a href="/user/login">Login</a></li>';
            }
         else{
            ?>

                        <div class="navbar-form navbar-left dropdown">
                            <button class="btn btn-danger dropdown-toggle" type="button" onclick="$('#posten')" title="klicken" data-toggle="dropdown">Neuer Post <span class="caret"></span></button>

                            <ul class="dropdown-menu">
                                <li>
                                    <div id="posten">
                                        <form action="/Galerie/doCreate" method="POST" enctype="multipart/form-data">
                                            <p>
                                                <input type="text" name="titel" class="form-control" placeholder="Titel">
                                            </p>

                                            <p>
                                                <textarea rows="4" cols="55" name="inhalt" class="form-control" placeholder="Inhalt"></textarea>
                                            </p>
                                            <p>
                                                <input type="file" title="png, jpg, jpeg, gif" name="datei" size="60" maxlength="255">

                                            </p>
                                            <p>
                                                <select name="kategorie" class="tab">
                <option value="%">Kategorie auswählen</option>
                <option value="Sport">Sport</option>
                <option value="Essen">Essen</option>
                <option value="Menschen">Menschen</option>
                <option value="Adventure">Adventure</option>
                <option value="Natur">Natur</option>
            </select>
                                            </p>
                                            <p>
                                                <input type="submit" name="submit" value="Posten" class="btn btn-default">
                                            </p>

                                        </form>
                                        <!-- Liest den geposteten Blog aus-->
                                    </div>
                                </li>
                            </ul>


                        </div>
                    </li>

                    <form class="navbar-form navbar-left" action="/Galerie/doSortieren" <?php echo $_SERVER[ 'PHP_SELF']; ?> method="post">
                        <div class="form-group">
                            <?php $kategoriegesetzt = (isset($_POST['kategorie'])?$_POST['kategorie']:"");?>
                            <select name="kategorie" class="form-control">
                <option value="%"<?php if($kategoriegesetzt == "Alle") echo "selected=\"selected\""; ?> />Alle
                <option value="Sport"<?php if($kategoriegesetzt == "Sport") echo "selected=\"selected\""; ?> />Sport
                <option value="Essen"<?php if($kategoriegesetzt == "Essen") echo "selected=\"selected\""; ?> />Essen
                <option value="Menschen"<?php if($kategoriegesetzt == "Menschen") echo "selected=\"selected\""; ?> />Menschen
                <option value="Adventure"<?php if($kategoriegesetzt == "Adventure") echo "selected=\"selected\""; ?> />Adventure
                <option value="Natur"<?php if($kategoriegesetzt == "Natur") echo "selected=\"selected\""; ?> />Natur
            </select>
                        </div>
                        <input type="submit" title="klicken" name="sort" class="btn btn-default" value="Sortieren"><br/>
                    </form>



                </ul>
                <!--<form class="navbar-form navbar-left">
                            <div class="form-group">

                                <input type="text" class="form-control" placeholder="Search">
                            </div>
                            <button type="submit" class="btn btn-default">Submit</button>
                        </form>-->
                <ul class="nav navbar-nav navbar-right">

                    <li>



                        <?php echo '<li class="active"><a>'.'Hallo '.$_SESSION['username'].'</a></li>';
                        //Bezieht sich auf SESSION während man angemeldet ist
                        echo '<li><a href="'.$path.'">'.$description.'</a></li>';
             
            ?>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->

        </nav>



        <?php
         }
            
            
            ?>
    </div>
</header>





    <?php if (empty($Galeries)): ?>

    <h2 class="container">Kein Beitrag</h2>
    <?php else: ?>
    <?php $blog_id =0;?>

    <?php foreach ($Galeries as $Galerie): ?>

    <?php if($Galerie->blog_id != $blog_id){
                        if($blog_id > 0){
                               echo "</div></div>";
                        } 
                        $blog_id = $Galerie->blog_id;?>

    <div class="container" >
        <div class="clearfix visible-xs-block"></div>
    <div class="panel panel-default">
       
        <div class="panel-heading">

            <h2>
                <?= $Galerie->titel;?>
            </h2>
        </div>
    </div>
        <div class="panel-body"><strong>Blog by <?=$Galerie->username;?></strong>

<div class="col-md-4">
    <div class="thumbnail">
      <a href="<?= $Galerie->bild;?>" target="_blank"><img style="width:100%" class="img-responsive"  src="/<?= $Galerie->bild;?>">
          </a>
        <div class="caption">
          <p><?= $Galerie->inhalt;?></p>
        </div>
      
    </div>
    
  </div>
     
           <div class="caption">
                Datum:
                <?= $Galerie->datum;?>
           
            </div>
             <br>



            <div class="caption">
                <strong>Kategorie:</strong>
                <br>
                <?= $Galerie->kategorie;?>
            </div>
             <br>

            <?php 
    if($Galerie->id_user == $_SESSION['user_id']){?>


            <a class="btn btn-md btn-lg" href="/Galerie/delete?id=<?= $Galerie->blog_id ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;Löschen </a>


            <a class="btn btn-md btn-lg" href="/Galerie/edit?id=<?= $Galerie->blog_id;?>"><span class="glyphicon glyphicon-pencil"></span> &nbsp;Bearbeiten</a>
  
        
            
            
            
            

            <?php }?>
            
            <button class="btn btn-md btn-primary" type="button" onclick="$('#popup<?= $Galerie->blog_id;?>').slideToggle();"><span class="glyphicon glyphicon-comment "></span>&nbsp;Kommentieren</button>
            
                <div class="collapse" id="popup<?= $Galerie->blog_id;?>"  hidden>
               
                    
                    <h2>Kommentieren:</h2>
                    <FORM ACTION="/Galerie/doKommentieren" METHOD="post">
                        <div class="container">
                       
                        <TEXTAREA name="kommentar"  style="margin: 0px; height: 5em; width: 20em;"></TEXTAREA>
                        <input type="hidden" name="id_blog" value="<?= $Galerie->blog_id;?>" />
                  <div class="offset-sm-2 col-sm-10">
                        <INPUT class="btn btn-default" TYPE="submit" VALUE="Kommentieren" class="btn btn-success col-sm" name="submit" />
                            </div>
                          
                        </div>
                        
                    </FORM>
                        </div>
             
               
           

            
            <h3>Kommentare</h3>
            <?php if ( !empty($Galerie->kommentar) ) { ?>
            <p id="kommentar-text">
                <p><strong>Comment by
                                <?=$Galerie->kommentaruser;?></strong>


                    <?php if ($Galerie->kommentaruserid == $_SESSION['user_id'] || $Galerie->id_user == $_SESSION['user_id'] ){?>
                    <a href="/Galerie/deletekommentar?id=<?= $Galerie->kommentar_id ?>"><img src="/view/css/img/poubelle.png" width="15em"></a>
                    <?php }?>
                </p>
                <?=$Galerie->kommentar;?>

            </p>
            <?php } ?>



            <?php  }else {?>
            <p id="kommentar-text">
                <p><strong>Comment by
                                <?=$Galerie->kommentaruser;?></strong>


                    <?php if ($Galerie->kommentaruserid == $_SESSION['user_id'] || $Galerie->id_user == $_SESSION['user_id'] ){?>
                    <a href="/Galerie/deletekommentar?id=<?= $Galerie->kommentar_id ?>"><img src="/view/css/img/poubelle.png" width="15em"></a>
                    <?php }?>
                </p>
                <?=$Galerie->kommentar;?>

            </p>

            <?php      }?>



            <?php endforeach ?>

            <?php if($blog_id > 0){
                               echo " </div></div>";
                        } ?>
            <?php endif ?>


        </div>
</div>

