<?php

require_once 'lib/Model.php';

/**
 * Das UserModel ist zuständig für alle Zugriffe auf die Tabelle "user".
 *
 * Die Ausführliche Dokumentation zu Models findest du in der Model Klasse.
 */
class UserModel extends Model
{
    /**
     * Diese Variable wird von der Klasse Model verwendet, um generische
     * Funktionen zur Verfügung zu stellen.
     */
    protected $tableName = 'user';

    /**
     * Erstellt einen neuen benutzer mit den gegebenen Werten.
     *
     * Das Passwort wird vor dem ausführen des Queries noch mit dem SHA1
     *  Algorythmus gehashed.
     *
     * @param $firstName Wert für die Spalte firstName
     * @param $lastName Wert für die Spalte lastName
     * @param $email Wert für die Spalte email
     * @param $password Wert für die Spalte password
     *
     * @throws Exception falls das Ausführen des Statements fehlschlägt
     */
    public function create($username, $alter, $password)
    {
        $password = sha1($password);

        $query = "INSERT INTO $this->tableName (name, age, passwort) VALUES (?, ?, ?)";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param('sis', $username, $alter, $password);

        if (!$statement->execute()) {
            throw new Exception($statement->error);
        }
    }
    
    public function check($username, $password){
        $password = sha1($password);

        $query = "SELECT name, user_id FROM $this->tableName WHERE name=? and passwort=?";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param('ss', $username, $password);

                // Das Statement absetzen
        $statement->execute();
       $result = $statement->get_result();
        if (!$result) {
            throw new Exception($statement->error);
        }

        // Ersten Datensatz aus dem Reultat holen
        
        while($row = $result->fetch_object()){
            $_SESSION['user_id']=$row->user_id;
            if(strlen($row->name)>0){
                
                $_SESSION['username']=$row->name;
                $_SESSION['user_id']=$row->user_id;
            }
        }
        
     
        
        
    }
}
