<?php

require_once 'lib/Model.php';

/**
 * Das UserModel ist zuständig für alle Zugriffe auf die Tabelle "user".
 *
 * Die Ausführliche Dokumentation zu Models findest du in der Model Klasse.
 */
class KommentarModel extends Model
{
    /**
     * Diese Variable wird von der Klasse Model verwendet, um generische
     * Funktionen zur Verfügung zu stellen.
     */
    protected $tableName = 'kommentar';

    /**
     * Erstellt einen neuen benutzer mit den gegebenen Werten.
     *
     * Das Passwort wird vor dem ausführen des Queries noch mit dem SHA1
     *  Algorythmus gehashed.
     *
     * @param $firstName Wert für die Spalte firstName
     * @param $lastName Wert für die Spalte lastName
     * @param $email Wert für die Spalte email
     * @param $password Wert für die Spalte password
     *
     * @throws Exception falls das Ausführen des Statements fehlschlägt
     */

    
    public function kommentieren($kommentar, $id_blog, $id_user)
    {
        //$password = sha1($password);

        $query = "INSERT INTO $this->tableName (kommentar, id_blog, id_user) VALUES (?,?,?)";
        
$kommentar = strip_tags ($kommentar);
        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param('sii', $kommentar, $id_blog, $id_user);

        if (!$statement->execute()) {
            throw new Exception($statement->error);
        }
    }
    
    
    public function deleteByKid($id)
    {
        $query = "DELETE FROM $this->tableName WHERE kommentar_id=?";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param('i', $id);

        if (!$statement->execute()) {
            throw new Exception($statement->error);
        }
    }
    
    public function deleteByFK($id)
    {
        $query = "DELETE FROM $this->tableName WHERE id_blog=?";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param('i', $id);

        if (!$statement->execute()) {
            throw new Exception($statement->error);
        }
    }

}
