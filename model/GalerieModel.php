<?php

require_once 'lib/Model.php';

/**
 * Das UserModel ist zuständig für alle Zugriffe auf die Tabelle "user".
 *
 * Die Ausführliche Dokumentation zu Models findest du in der Model Klasse.
 */
class GalerieModel extends Model
{
    /**
     * Diese Variable wird von der Klasse Model verwendet, um generische
     * Funktionen zur Verfügung zu stellen.
     */
    protected $tableName = 'beitrag';

    /**
     * Erstellt einen neuen benutzer mit den gegebenen Werten.
     *
     * Das Passwort wird vor dem ausführen des Queries noch mit dem SHA1
     *  Algorythmus gehashed.
     *
     * @param $firstName Wert für die Spalte firstName
     * @param $lastName Wert für die Spalte lastName
     * @param $email Wert für die Spalte email
     * @param $password Wert für die Spalte password
     *
     * @throws Exception falls das Ausführen des Statements fehlschlägt
     */
    public function create($titel, $inhalt, $kategorie, $id_user, $new_path)
    {
        //$password = sha1($password);

        $query = "INSERT INTO $this->tableName (titel, inhalt, kategorie, id_user, bild) VALUES (?,?,?,?,?)";
            $titel = strip_tags ($titel);
            $inhalt = strip_tags ($inhalt);

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param('sssis', $titel, $inhalt, $kategorie, $id_user, $new_path);

        if (!$statement->execute()) {
            throw new Exception($statement->error);
        }
    }
    
    public function readAllInclude($max=100){
            
        $query = "SELECT b.*, u.name as username, k.kommentar_id, k.kommentar as kommentar, k.id_user as kommentaruserid, ku.name as kommentaruser FROM $this->tableName as b join user as u on (b.id_user=u.user_id) left join kommentar as k on (b.blog_id=k.id_blog) left join user as ku on (k.id_user=ku.user_id) order by  datum  desc limit 0, $max" ;

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->execute();

        $result = $statement->get_result();
        if (!$result) {
            throw new Exception($statement->error);
        }

        // Datensätze aus dem Resultat holen und in das Array $rows speichern
        $rows = array();
        while ($row = $result->fetch_object()) {
            $rows[] = $row;
        }

        return $rows;
    
    }
    
        public function edit($titel, $inhalt, $kategorie, $id_user, $blog_id, $new_path)
    {
        //$password = sha1($password);

        $query = "UPDATE $this->tableName set titel=?, inhalt=?, kategorie=?, bild=? where blog_id=?";
            
             $titel = strip_tags ($titel);
            $inhalt = strip_tags ($inhalt);

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param('ssssi', $titel, $inhalt, $kategorie, $new_path, $blog_id);

        if (!$statement->execute()) {
            throw new Exception($statement->error);
        }
    }
    
            public function sortieren($kategorie, $max=100){
        $query = "SELECT b.*, u.name as username, k.kommentar_id, k.kommentar as kommentar, k.id_user as kommentaruserid, ku.name as kommentaruser FROM $this->tableName as b join user as u on (b.id_user=u.user_id) left join kommentar as k on (b.blog_id=k.id_blog) left join user as ku on (k.id_user=ku.user_id) where kategorie like ? order by  datum  desc limit 0, $max" ;

        $statement = ConnectionHandler::getConnection()->prepare($query);
                $statement->bind_param('s', $kategorie);
        $statement->execute();

        $result = $statement->get_result();
        if (!$result) {
            throw new Exception($statement->error);
        }

        // Datensätze aus dem Resultat holen und in das Array $rows speichern
        $rows = array();
        while ($row = $result->fetch_object()) {
            $rows[] = $row;
        }

        return $rows;
    
    }
    

    
    
    
      /*  public function kommentieren($kommentar, $id_blog, $id_user)
    {
        //$password = sha1($password);

        $query = "INSERT INTO $this->tableName (titel, inhalt, kategorie, id_user) VALUES (?,?,?,?)";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param('sssi', $titel, $inhalt, $kategorie, $id_user);

        if (!$statement->execute()) {
            throw new Exception($statement->error);
        }
    }*/
    
    

}
