<?php

require_once 'model/UserModel.php';

/**
 * Siehe Dokumentation im DefaultController.
 */
class UserController
{
    public function index()
    {
        $userModel = new UserModel();

        $view = new View('default_index');
        $view->title = 'Benutzer';
        $view->heading = 'Benutzer';
        $view->users = $userModel->readAll();
        $view->display();
    }
public function login()
{
        $view = new View('default_index');
        $view->title = 'Benutzer eingeloggt';
        $view->heading = 'Benutzer eingeloggt';
        $view->display();
  
}
    public function create()
    {
        $view = new View('default_index');
        $view->title = 'Benutzer erstellen';
        $view->heading = 'Benutzer erstellen';
        $view->display();
    }

    public function doCreate() //erstellt User
    {
        if ($_POST['submit']) {
            $username = $_POST['username'];
            $alter = $_POST['alter'];
            $password = $_POST['password'];
            $password2 = $_POST['password2'];

            $userModel = new UserModel();
            $create=$userModel->create($username, $alter, $password, $password2);
        }
        

        // Anfrage an die URI /user weiterleiten (HTTP 302)
        header('Location: /galerie');
    }
    
    public function doLogin() //Erstellt login 
    {
        var_dump($_SESSION);
        if ($_POST['submit']) {
            $username = $_POST['username'];
            $password = $_POST['password'];

            $userModel = new UserModel();
            
            $userModel->check($username, $password);
        }
        
        // Anfrage an die URI /user weiterleiten (HTTP 302)
        if(isset ($_SESSION ["user_id"])){ //Wird nur eingelogt wenn die ID in der Datenbank ist
            header('Location: /galerie');
        }else{ //Sonst verweis auf Login Seite
            header('Location: /');
            
        }
        
    }
    
    public function doLogout(){ //Session wird geschlossen 
        session_destroy();
        unset($_SESSION);
        
        header('Location: /'); //Verweist auf default Seite
    }

    public function delete()
    {
        $userModel = new UserModel();
        $userModel->deleteById($_GET['user_id']);

        // Anfrage an die URI /user weiterleiten (HTTP 302)
        header('Location: /user');
    }
}
