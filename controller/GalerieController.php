<?php

require_once 'model/GalerieModel.php';
require_once 'model/KommentarModel.php';
/**
 * Siehe Dokumentation im DefaultController.
 */
class GalerieController
{
    public function index()
    {
        if(session_id()&&isset($_SESSION["user_id"])){
        $GalerieModel = new GalerieModel();

        $view = new View('Galerie_index');
        $view->title = 'Galerie';
        $view->heading = 'Galerie';
        $view->Galeries = $GalerieModel->readAllInclude();
        $view->display();}
        else{
            header('Location: /user/login');
        }
    }

    public function create()
    {
        $view = new View('Galerie_index');
        $view->title = 'Blog erstellen';
        $view->heading = 'Blog erstellen';
        $view->display();
    }
    
        public function sortieren()
    {
        $view = new View('Galerie_index');
        $view->title = 'Sortieren';
        $view->heading = 'Sortieren';
        $view->display();
    }
    
        public function edit()
    {
            
        $GalerieModel = new GalerieModel();
        $view = new View('Galerie_edit');
        $view->title = 'Blog bearbeiten';
        $view->heading = 'Blog bearbeiten';
        $view->Galerie = $GalerieModel->readByID($_GET["id"]);
        $view->display();
    }
    
   /*         public function kommentieren()
    {
            
        $GalerieModel = new GalerieModel();
        $view = new View('Galerie_index');
        $view->title = 'Blog bearbeiten';
        $view->heading = 'Blog bearbeiten';
        $view->Galerie = $GalerieModel->readByID($_GET["blog_id"]);
        $view->display();
    }*/
    
            public function doEdit()
    {
        if ($_POST['submit']) {
            $titel = $_POST['titel'];
            $inhalt = $_POST['inhalt'];
            $kategorie = $_POST['kategorie'];
            $id_user = $_SESSION['user_id'];
            $id = $_POST["idhidden"];
            
             require "dateiupload.php";
          
        
            $GalerieModel = new GalerieModel();
            $edit=$GalerieModel->edit($titel, $inhalt, $kategorie, $id_user, $id, $new_path);
        }

        // Anfrage an die URI /user weiterleiten (HTTP 302)
       header('Location: /Galerie');
    }


    public function doCreate()
    {
        if ($_POST['submit']) {
            $titel = $_POST['titel'];
            $inhalt = $_POST['inhalt'];
            $kategorie = $_POST['kategorie'];
            $id_user = $_SESSION['user_id'];
            
            require "dateiupload.php";
        
            $GalerieModel = new GalerieModel();   
            $create=$GalerieModel->create($titel, $inhalt, $kategorie, $id_user, $new_path);
        }

        // Anfrage an die URI /user weiterleiten (HTTP 302)
        header('Location: /Galerie');
    }
    
        public function doSortieren()
    {
            if(session_id()&&isset($_SESSION["user_id"])){
        if ($_POST['kategorie']) {
            $kategorie = $_POST['kategorie'];       
            $GalerieModel = new GalerieModel();   
            $create=$GalerieModel->sortieren($kategorie);

        
            $GalerieModel = new GalerieModel();

            $view = new View('Galerie_index');
            $view->title = 'Galerie';
            $view->heading = 'Galerie';
            $view->Galeries = $GalerieModel->sortieren($kategorie);
            $view->display();
        } else{
            header('Location: /Galerie');
        }}       else{
            header('Location: /user/login');
        }
    }
    
    
        public function doKommentieren()
    {
           if($_POST['submit']){
            $kommentar = $_POST['kommentar'];
            $id_blog = $_POST['id_blog'];
            $id_user =  $_SESSION['user_id'];
            
            $KommentarModel = new KommentarModel();
            $KommentarModel->kommentieren($kommentar, $id_blog, $id_user);
           }
           header("Location: /Galerie");

        // Anfrage an die URI /user weiterleiten (HTTP 302)
    }

    public function delete()
    {
        $KommentarModel = new KommentarModel();
         $KommentarModel->deleteByFK($_GET['id']);
        
        $GalerieModel = new GalerieModel();
        $GalerieModel->deleteById($_GET['id']);

        // Anfrage an die URI /user weiterleiten (HTTP 302)
        header('Location: /Galerie');
    }
    
        public function deletekommentar()
    {
        $KommentarModel = new KommentarModel();
        $KommentarModel->deleteByKid($_GET['id']);

        // Anfrage an die URI /user weiterleiten (HTTP 302)
        header('Location: /Galerie');
    }
}
