-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 11. Jun 2016 um 18:44
-- Server-Version: 10.1.9-MariaDB
-- PHP-Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `bilderdb`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `beitrag`
--

CREATE TABLE `beitrag` (
  `blog_id` int(11) NOT NULL,
  `titel` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `datum` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `inhalt` text CHARACTER SET latin1,
  `kategorie` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `bild` varchar(200) NOT NULL,
  `galerie` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `beitrag`
--

INSERT INTO `beitrag` (`blog_id`, `titel`, `datum`, `inhalt`, `kategorie`, `id_user`, `bild`, `galerie`) VALUES
(92, 'Mein erster Blog', '2016-04-22 12:21:59', 'Glücklich', 'Action', 22, 'upload/Koala_2.jpg', ''),
(96, 'k', '2016-05-26 06:57:04', 'faadfs                                ', 'Shooter', 39, 'upload/Desert.jpg', ''),
(97, NULL, '2016-06-02 07:26:56', NULL, 'Adventure', 39, 'upload/Lighthouse_5.jpg', ''),
(98, NULL, '2016-06-02 07:30:09', NULL, 'Action', 39, 'upload/Penguins_3.jpg', ''),
(99, NULL, '2016-06-02 07:32:38', NULL, '%', 39, 'upload/Penguins_4.jpg', ''),
(100, NULL, '2016-06-02 07:39:00', NULL, 'Shooter', 39, 'upload/Lighthouse_6.jpg', ''),
(101, NULL, '2016-06-02 10:27:47', NULL, 'Adventure', 39, 'upload/Penguins_5.jpg', ''),
(102, NULL, '2016-06-05 15:21:56', NULL, 'Sport', 39, 'upload/Lighthouse_7.jpg', ''),
(103, NULL, '2016-06-05 15:26:10', NULL, 'Action', 39, 'upload/Koala_6.jpg', ''),
(104, 'dsads', '2016-06-11 16:36:28', 'adassda', '%', 39, 'upload/Tulips_1.jpg', ''),
(105, 'dsadsa', '2016-06-11 16:36:41', 'dsadsa', 'Shooter', 39, 'upload/Penguins.jpg', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `galerie`
--

CREATE TABLE `galerie` (
  `galerie_id` int(11) NOT NULL,
  `name` varchar(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kommentar`
--

CREATE TABLE `kommentar` (
  `kommentar_id` int(11) NOT NULL,
  `kommentar` text CHARACTER SET latin1 NOT NULL,
  `id_blog` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `kommentar`
--

INSERT INTO `kommentar` (`kommentar_id`, `kommentar`, `id_blog`, `id_user`) VALUES
(1, 'fdfs\r\n', 96, 39);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `passwort` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`user_id`, `name`, `age`, `passwort`) VALUES
(16, 'martin', 18, 'c9198296f907c066bd972d59eace9af1402a4eea'),
(22, 'Alex', 18, 'd91779eb6f0f188c5d12f133f197ebd5e5775b07'),
(25, 'Jonas', 18, 'a88e01df24a80d2b5a560a19af25c166bc21ffb9'),
(39, 'martin', 18, '2cbd66da43ef5ceb90166f7ca11bc9a1c69f09ac');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `beitrag`
--
ALTER TABLE `beitrag`
  ADD PRIMARY KEY (`blog_id`),
  ADD KEY `kommentar_ibfk_1` (`id_user`);

--
-- Indizes für die Tabelle `kommentar`
--
ALTER TABLE `kommentar`
  ADD PRIMARY KEY (`kommentar_id`),
  ADD KEY `fk_beitrag` (`id_blog`),
  ADD KEY `fk_user` (`id_user`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `beitrag`
--
ALTER TABLE `beitrag`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT für Tabelle `kommentar`
--
ALTER TABLE `kommentar`
  MODIFY `kommentar_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `beitrag`
--
ALTER TABLE `beitrag`
  ADD CONSTRAINT `beitrag_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`user_id`),
  ADD CONSTRAINT `kommentar_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`user_id`);

--
-- Constraints der Tabelle `kommentar`
--
ALTER TABLE `kommentar`
  ADD CONSTRAINT `fk_beitrag` FOREIGN KEY (`id_blog`) REFERENCES `beitrag` (`blog_id`),
  ADD CONSTRAINT `fk_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
