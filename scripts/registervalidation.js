function checkEmail(){
	var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	var valid = re.test($("#email").val());

	if (!valid){
		$("#errorEmail").html("<i style=\"color: red;\">Geben Sie eine gültige E-Mail Adresse an.</i><br />");
		return false;
	}
	else {
		$("#errorEmail").html("");
		return true;
	}
}

function checkUsername(){
	if ($("#benutzername").val().length < 4){
		$("#errorUsername").html("<i style=\"color: red;\">Der Benutzername muss mindestens 4 Zeichen lang sein.</i><br />");
		return false;
	}
	else{
		$("#errorUsername").html("");
		return true;
	}
}

function checkPasswort(){
	var re = /(?=.*[a-z])(?=.*[A-Z])(?=.*[()[\]{}?!$%&/=*+~,.;:<>\-_]).{9,}/g;
	if ($("#passwort").val().length < 9){
		$("#errorPasswort").html("<i style=\"color: red;\">Das Passwort muss mindestens ein Sonderzeichen (( ) [ ] { } ? ! $ % & / = * + ~ , . ; : < > - _ ), einen Grossbuchstaben und einen Kleinbuchstaben enthalten, sowie mindestens 9 Zeichen lang sein.</i><br />");
		return false;
	}
	else{
		if (!re.test($("#passwort").val())){
			$("#errorPasswort").html("<i style=\"color: red;\">Das Passwort muss mindestens ein Sonderzeichen (( ) [ ] { } ? ! $ % & / = * + ~ , . ; : < > - _ ), einen Grossbuchstaben und einen Kleinbuchstaben enthalten, sowie mindestens 9 Zeichen lang sein.</i><br />");
			return false;
		}
		else {
			$("#errorPasswort").html("");
			checkPasswort2();
			return true;
		}
	}
}

/*function checkPasswort2(){
	if ($("#passwort").val() != $("#passwort2").val()){
		$("#errorPasswort2").html("<i style=\"color: red;\">Die beiden Passwörter stimmen nicht überein.</i><br />");
		return false;
	}
	else {
		$("#errorPasswort2").html("");
		return true;
	}
}*/

function checkCheckboxes(){
	return ($("#checkbox1").prop('checked') || $("#checkbox2").prop('checked') || $("#checkbox3").prop('checked') || $("#checkbox4").prop('checked'));
}

function validateRegister(){
	return (checkEmail() && checkUsername() && checkPasswort() && checkPasswort2() && checkCheckboxes());
}

function res(){
	$("#email").prop('value', "");
	$("#benutzername").prop('value', "");
	$("#passwort").prop('value', "");
	$("#passwort2").prop('value', "");
	$("#optionsRadio1").prop('checked', false);
	$("#optionsRadio2").prop('checked', false);
	$("#optionsRadio3").prop('checked', false);
	$("#optionsRadio4").prop('checked', false);
	$("#checkbox1").prop('checked', false);
	$("#checkbox2").prop('checked', false);
	$("#checkbox3").prop('checked', false);
	$("#checkbox4").prop('checked', false);
	$("#auswahl").val('1');
	$("#errorUsername").html("");
	$("#errorPasswort").html("");
	$("#errorPasswort2").html("");
}

function validateLogin(){
	return true;
}

$("#resetButton").click(function () {
	modalYesNo(function (yes) {
		if (yes) {
			res();

			$('html, body').animate({ scrollTop: (0) }, 'normal', function () {
				$(".alertClose").parent().fadeOut(400);
				return true;
			});
		}
	});

	return false;
});

/*function modalYesNo(callback) {
	$("#modelZurueck").fadeIn(300);

	var btnYes = $(".modal-btn-yes");
	var btnNo = $(".modal-btn-no");

	btnYes.click(function () { $(".modal-background").fadeOut(300); callback(true); });
	btnNo.click(function () { $(".modal-background").fadeOut(300); callback(false); });
}

$('.modalOk').click(function () {
	$("#modalDaten").fadeOut(300);
});*/
